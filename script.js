﻿var sin=true,cos=false,tan=false,ctg=false;
function sing(){
	sin=true;
	cos=false;
	tan=false;
	ctg=false;
	document.getElementById("szögf").innerHTML="sin(x)=";	
}
function cosg(){
	sin=false;
	cos=true;
	tan=false;
	ctg=false;
	document.getElementById("szögf").innerHTML="cos(x)=";		
} 
function tang(){
	sin=false;
	cos=false;
	tan=true;
	ctg=false;
	document.getElementById("szögf").innerHTML="tan(x)=";			
} 
function ctgg(){
	sin=false;
	cos=false;
	tan=false;
	ctg=true;
	document.getElementById("szögf").innerHTML="ctg(x)=";			
}  
function szamolas(){
	var szam=document.getElementById("szam").value;
	szam=szam.replace(",",".");
	szam=Number(szam);
	var x1,x2;
	if(isNaN(szam)) 
	{
		document.getElementById("x1").innerHTML="Helytelen adat";
		document.getElementById("x2").innerHTML="";
	}
	if(sin){
		if(0<szam  && szam<1){
			x1=Math.asin(szam);
			x1=x1 * (180 / Math.PI);
			x2=180-x1;
			document.getElementById("x1").innerHTML=("x<sub>1</sub>="+x1.toFixed(4)+"+k*360");
			document.getElementById("x2").innerHTML=("x<sub>2</sub>="+x2.toFixed(4)+"+l*360");
		}
		if(-1<szam  && szam<0){
			szam=szam*-1;
			x1=Math.asin(szam);
			x1=x1 * (180 / Math.PI);
			x2=360-x1;
			x1=x1+180;
			document.getElementById("x1").innerHTML=("x<sub>1</sub>="+x1.toFixed(4)+"+k*360");
			document.getElementById("x2").innerHTML=("x<sub>2</sub>="+x2.toFixed(4)+"+l*360");
		}
		if(szam==0){
			document.getElementById("x1").innerHTML="x<sub>1</sub>=0+k*360";
			document.getElementById("x2").innerHTML="x<sub>2</sub>=360+l*360";
		}
		if(szam==1){
			document.getElementById("x1").innerHTML="x<sub>1</sub>=90+k*360";
			document.getElementById("x2").innerHTML="";
		}
		if(szam==-1){
			document.getElementById("x1").innerHTML="x<sub>1</sub>=270+l*360";
			document.getElementById("x2").innerHTML="";
		}
	}
	if(cos){
		if(0<szam  && szam<1){
			x1=Math.acos(szam);
			x1=x1 * (180 / Math.PI);
			x2=360-x1;
			document.getElementById("x1").innerHTML=("x<sub>1</sub>="+x1.toFixed(4)+"+k*360");
			document.getElementById("x2").innerHTML=("x<sub>2</sub>="+x2.toFixed(4)+"+l*360");
		}
		if(-1<szam  && szam<0){
			szam=szam*-1;
			x1=Math.acos(szam);
			x1=x1 * (180 / Math.PI);
			x2=180-x1;
			x1=x1-180;
			document.getElementById("x1").innerHTML=("x<sub>1</sub>="+x1.toFixed(4)+"+k*360");
			document.getElementById("x2").innerHTML=("x<sub>2</sub>="+x2.toFixed(4)+"+l*360");
		}
		if(szam==0){
			document.getElementById("x1").innerHTML="x<sub>1</sub>=90+k*360";
			document.getElementById("x2").innerHTML="x<sub>2</sub>=270+l*360";
		}
		if(szam==1){
			document.getElementById("x1").innerHTML="x<sub>1</sub>=0+k*360";
			document.getElementById("x2").innerHTML="x<sub>2</sub>=360+l*360";
		}
		if(szam==-1){
			document.getElementById("x1").innerHTML="x<sub>1</sub>=180+l*360";
			document.getElementById("x2").innerHTML="";
		}
	}
	if(tan){
			x1= Math.atan(szam);
			x1=x1 * (180 / Math.PI);
			document.getElementById("x1").innerHTML=("x<sub>1</sub>="+x1.toFixed(4)+"+k*180");
			document.getElementById("x2").innerHTML="";
		
	}
	if(ctg){
			x1= Math.atan(1/szam);
			x1=x1 * (180 / Math.PI);
			document.getElementById("x1").innerHTML=("x<sub>1</sub>="+x1.toFixed(4)+"+k*180");
			document.getElementById("x2").innerHTML="";
		
		}
}